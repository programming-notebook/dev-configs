Note: This document expects the current repo is checked out in `$HOME/.dev-configs` or `$HOME/.dev-configs/` points to the repo checkout location

# Source the `bash/rc`
To use the bash configuration/settings source it in your `~/.bashrc` file by adding following code

```
if [ -r ~/.dev-configs/bash/rc ]; then
   source ~/.dev-configs/bash/rc
fi
```