set bg=dark

set nocompatible

" Search
set ignorecase 
set incsearch  "Incremental
set hlsearch " Highlight

set ruler
set shell=bash

set showmatch " matching block
set showmode "working mode
set showcmd

set cmdheight=1 "command-line size
set magic "Patten matching
set bs=eol,indent,start  "backspace in insert mode

set expandtab
set tabstop=8 "Number of spaces for a tab
set nowrap
set nowrapscan "do not wrap search
set smartindent

set noequalalways "Close a split window in Vim without resizing other windows
set warn
set nowriteany
set listchars=tab:>-,trail:-

set laststatus=2 " Always show status line

syntax on " Turn on syntax highlighting

filetype plugin on " Loading plugin for specific file types
# file type
autocmd BufNewFile,BufRead *.strace setl filetype=strace
autocmd BufNewFile,BufRead *.c,*.h,*.C,*.cc,*.cpp,*.hpp,*.cxx,*.hxx,*.hh,*.ipp  setl filetype=cpp
autocmd BufNewFile,BufRead makefile*,Makefile*,*.mk  setl filetype=make
autocmd BufNewFile,BufRead *.am  setl filetype=automake

autocmd FileType cpp setl syntax=cpp11
autocmd FileType cpp highlight Member term=bold ctermfg=white gui=bold
autocmd FileType cpp syntax match Member /\<m_[_A-Za-z0-9]*\>/

map - <C-W>-  "- decrease the current window height
map = <C-W>+  "= increase the current window height
map _ <C-W><  "_ to decrease the current window width
map + <C-W>>  "+ to increase the current window width
