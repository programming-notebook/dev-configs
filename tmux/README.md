Note: This document expects the current repo is checked out in `$HOME/.dev-configs` or `$HOME/.dev-configs/` points to the repo checkout location

# Source the 'tmux/conf'
To use the tmux configuration from `tmux/conf` source it in your `~/.tmux.conf` file by adding following directive

```
source-file ~/.dev-configs/tmux/conf
```