Note: This document expects the current repo is checked out in `$HOME/.dev-configs` or `$HOME/.dev-configs/` points to the repo checkout location

# Adding "aliases" in gitconfig

To make use of these git aliases use one of following step
* If you have existing `[include]` block/s then add following additional block in your `~/.gitconfig` 

```
[include]
    path=~/.dev-configs/git/aliases
```

* or if you don't have existing [include] then run
```bash
$ git config --global include.path ~/.dev-config/git/aliases
```

# Add git-bashcompletion.bash
To have nice git bash completions, download [git-bashcompletion.bash](https://github.com/git/git/blob/master/contrib/completion/git-completion.bash) file in `~/.dev-configs/git/` folder

Alternatively this file can be found in your git instllation.

e.g.
``` bash
$ curl -o ~/.dev-configs/git/git-completion.bash https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
```

# Add git-prompt.sh
To have nice git bash completions, download [git-prompt.sh](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh) file in `~/.dev-configs/git/` folder

Alternatively this file can be found in your git instllation.

e.g.
``` bash
$ curl -o ~/.dev-configs/git/git-prompt.sh https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh
```